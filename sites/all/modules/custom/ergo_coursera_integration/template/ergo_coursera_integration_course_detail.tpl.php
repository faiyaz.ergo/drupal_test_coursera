<?php if ( isset($items['course']['elements']) && !empty($items['course']['elements']) ) {
?>

<div id="course-details-page">

    <div class="course-header">
        <img src="<?php echo $items['course']['elements'][0]['photo']; ?>" alt="<?php echo $items['course']['elements'][0]['name']; ?>"/>
        <h1><?php echo $items['course']['elements'][0]['name']; ?></h1>
        <p><?php echo $items['course']['elements'][0]['shortDescription']; ?></p>
    </div>
    <hr>
    <div class="course-info">
        <div class="about-this-course">
            <?php echo $items['course']['elements'][0]['aboutTheCourse']; ?>
        </div>
    </div>
    <hr>
    <div class="course-other-info">

        <div class="unversity-info">
            <?php $university_logo = !empty($items['course']['linked']['universities'][0]['logo']) ? $items['course']['linked']['universities'][0]['logo'] : '/'.drupal_get_path('module', 'ergo_coursera_integration').'/resourece/images/no-university.png'; ?>
            <img src="<?php echo $university_logo ?>" alt="<?php echo $items['course']['linked']['universities'][0]['name']; ?>"/>
            <p><?php echo $items['course']['linked']['universities'][0]['name']; ?></p>
        </div>

        <?php if(!empty($items['course']['linked']['instructors'])):?>
        <hr>
        <h2>Instructor</h2>
        <div class="instructor-info">
            <img width="20%" src="<?php echo $items['course']['linked']['instructors'][0]['photo']; ?>" alt="<?php echo $items['course']['linked']['universities'][0]['name']; ?>"/>
            <p><?php echo $items['course']['linked']['instructors'][0]['firstName'].' '.$items['course']['linked']['instructors'][0]['lastName']; ?></p>
            <p><?php echo $items['course']['linked']['instructors'][0]['title']; ?></p>
            <p><?php echo $items['course']['linked']['instructors'][0]['bio']; ?></p>
        </div>
        <?php endif;?>

    </div>
    <?php if(!empty($items['course']['elements'][0]['faq'])): ?>
    <hr>
    <h2>FAQ</h2>
    <div class="faq">
        <?php echo $items['course']['elements'][0]['faq']; ?>
    </div>
    <?php endif;?>

</div>


<?php } ?>