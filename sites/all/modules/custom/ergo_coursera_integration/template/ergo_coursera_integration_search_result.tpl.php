<?php $query = isset($_SESSION['search_query']) ? $_SESSION['search_query'] : ""; ?>
<div id="search-form">
    <form id="search-form-id" action="/search-results" method="get">
        <input type="text" placeholder="What would you like to learn about?" size="30" name="query" value="<?php echo $query; ?>" id="search-query"/>
        <input type="submit" name="submit" value="Search"/>
    </form>
</div>

<?php
if ( isset($items['results']['elements']) && !empty($items['results']['elements']) ) {
    $total_results = count($items['results']['elements']);
    drupal_set_title($total_results . " Search Results Found.");
?>

    <div id="search-results">

        <div class="courses-list">
            <?php
            $i = 0;
            foreach ( $items['results']['elements'] as $course ) {
                ?>
                <div class="item">
                    <a href="/course/<?php echo $course['id']; ?>">
                        <div class="column">
                            <div class="content">
                                <img src="<?php echo $course['smallIcon']; ?>" alt="<?php echo $course['name']; ?>"/>
                                <h2><?php echo $course['name']; ?></h2>
                                <br>
                                <br>
                                <p><?php echo $course['shortDescription']; ?></p>
                            </div>
                        </div>
                    </a>
                </div>

                <?php
                $i++;
            }
            ?>
        </div>
    </div>

    </div>

<?php } else {

    drupal_set_title("0 Search Results Found.");

?>

<?php } ?>