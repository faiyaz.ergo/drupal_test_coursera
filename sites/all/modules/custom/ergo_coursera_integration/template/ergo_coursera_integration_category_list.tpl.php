<?php if ( isset($items['categories']['elements']) && !empty($items['categories']['elements']) ) { ?>
<div id="category-list">
    <div id="top-area" class="clearfix">
    <?php
    $i = 0;
    foreach( $items['categories']['elements'] as $category ) {
    ?>
        <?php if($i%3 == 0): ?>
            <div class="row-wrapper">
        <?php endif;?>

        <div class="column">
            <div class="content">
                <div class="feature"><i class="text-color fa fa-gears fa-3x"></i></div>
                <h2><?php echo $items['categories']['elements'][$i]['name']; ?></h2>

                <?php
                if ( !empty($items['categories']['elements'][$i]['description']) ) {
                    echo '<p>' . $items['categories']['elements'][$i]['description'] . '</p>';
                } else {
                    echo '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s</p>';
                }
                ?>
                <p><a class="btn btn-default" href="/category/<?php echo $items['categories']['elements'][$i]['id']; ?>">Read more</a></p>
            </div>
        </div>

        <?php if($i%3 == 2): ?>
            </div> 
            <!-- end row wrapper -->
        <?php endif;?>

    <?php
        $i++;
        }
    ?>
    </div>

</div>

<?php } else { ?>

    <div id="category-list">
        <div id="top-area" class="clearfix">
            <div class="content">
                <p>There is no category found.</p>
            </div>
        </div>

    </div>

<?php } ?>