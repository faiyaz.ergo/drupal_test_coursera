<?php if ( isset($items['categories']['elements']) && !empty($items['categories']['elements']) ) { ?>

    <div id="category-details">

        <div class="row header">
            <h1><?php echo $items['categories']['elements'][0]['name']; ?></h1>
            <p><?php echo $items['categories']['elements'][0]['description']; ?></p>
        </div>

        <div class="courses-list">
                <?php
                    $i = 0;
                    foreach ( $items['categories']['linked']['courses'] as $course ) {
                ?>
                        <div class="item">
                            <a href="/course/<?php echo $course['id']; ?>">
                                <div class="column">
                                    <div class="content">
                                        <img src="<?php echo $course['smallIcon']; ?>" alt="<?php echo $course['name']; ?>"/>
                                        <h2><?php echo $course['name']; ?></h2>
                                        <br>
                                        <br>
                                        <p><?php echo $course['shortDescription']; ?></p>
                                    </div>
                                </div>
                            </a>
                        </div>

                <?php
                        $i++;
                    }
                ?>
        </div>
        </div>

    </div>

<?php } else { ?>

    <div id="category-details">

        <div class="content">
            <p>Not found.</p>
        </div>

    </div>

<?php } ?>