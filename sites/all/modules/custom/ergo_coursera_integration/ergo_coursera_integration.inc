<?php

/**
 * Load helper function
 */
require_once('ergo_coursera_integration_helper.inc');


/**
 * Category details page
 */
function _coursera_category_detail( $catetory_id ) {

    // Make url
    $url = COURSERA_CATEGORIES_API.'?id=' . $catetory_id . '&fields=name,description,courses.fields(smallIcon,shortDescription)&includes=courses';

    // Get category information with course's info
    $category = ergo_coursera_integration_cURL($url);

    $items = array(
        'categories' => $category,
    );

    return theme( 'ergo_coursera_integration_category_detail', array('items' => $items) );
}


/**
 * Course details page
 */
function _coursera_course_detail($course_id) {

    //Make url
    $url = COURSERA_COURSES_API . '?id=' . $course_id . '&fields=name,photo,language,shortDescription,aboutTheCourse,faq,courseSyllabus,courseFormat,suggestedReadings,instructor,estimatedClassWorkload,aboutTheInstructor,universityLogo,universityLogoSt,universities.fields(name,logo),instructors.fields(photo,bio,title)&includes=universities,instructors,categories';

    //Get course detail
    $course_details = ergo_coursera_integration_cURL($url);

    $items = array(
        'course' => $course_details,
    );

    return theme( 'ergo_coursera_integration_course_detail', array('items' => $items) );
}